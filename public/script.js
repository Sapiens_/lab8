function clicked() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    if (!(/^[0-9]+$/).test(f1[0].value) || !(/^[0-9]+$/).test(f2[0].value)) {
        window.alert("Пожалуйста, вводите только числа!");
        return false;
    }
    r.innerHTML = "Сумма " + (f1[0].value * f2[0].value);
    return false;
}

function updatePrice() {
    // Находим select по имени в DOM.
    let s = document.getElementsByName("prodType");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }

    // Скрываем или показываем радиокнопки.
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = (select.value == "3" ? "block" : "none");

    // Смотрим какая товарная опция выбрана.
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        if (radio.checked) {
            let optionPrice = prices.prodOptions[radio.value];
            if (optionPrice !== undefined) {
                price += optionPrice;
            }
        }
    });

    // Скрываем или показываем чекбоксы.
    let checkDiv = document.getElementById("checkboxes");
    checkDiv.style.display = (select.value == "4" ? "block" : "none");

    // Смотрим какие товарные свойства выбраны

    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        if (checkbox.checked) {
            let propPrice = prices.prodProperties[checkbox.name];
            if (propPrice !== undefined) {
                price += propPrice;
            }
        }
    });
    let quanity = document.getElementsByName("quanity");
    quanity[0].value = quanity[0].value.replace(/[^0-9]/g, "");

    let prodPrice = document.getElementById("prodPrice");
    prodPrice.innerHTML = ((price) * (quanity[0].value)) + " рублей";
}


function getPrices() {
    return {
        prodTypes: [999, 3360, 4537],
        prodOptions: {
            option2: 980,
            option3: 1940,
        },
        prodProperties: {
            prop1: 990,
            prop2: 100,
        }
    };
}

window.addEventListener("DOMContentLoaded", function () {
    $('.gallery').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        //adaptiveHeight: true, //только при одном слайде
        responsive: [
            {
                breakpoint: 720, //мобильные
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    //adaptiveHeight: true
                }
      },
    ]
    });

    document.getElementById("quanity").onkeyup = updatePrice;

    let b = document.getElementById("button1");
    b.addEventListener("click", clicked);
    let r = document.getElementById("result");
    r.innerHTML = "Результат будет тут";

    // Скрываем радиокнопки.
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";

    // Находим select по имени в DOM.
    let s = document.getElementsByName("prodType");
    let select = s[0];
    // Назначаем обработчик на изменение select.
    select.addEventListener("change", function (event) {
        let target = event.target;
        console.log(target.value);
        updatePrice();
    });

    // Назначаем обработчик радиокнопок.  
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            let r = event.target;
            console.log(r.value);
            updatePrice();
        });
    });

    // Назначаем обработчик радиокнопок.  
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function (event) {
            let c = event.target;
            console.log(c.name);
            console.log(c.value);
            updatePrice();
        });
    });

    updatePrice();
});
window.addEventListener("DOMContentLoaded", function () {
    $(function () {
        $(".ajaxForm").submit(function (e) {
            e.preventDefault();
            var href = $(this).attr("action");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: href,
                data: $(this).serialize(),
                success: function (response) {
                    if (response.status == "success") {
                        alert("We received your submission, thank you!");
                    } else {
                        alert("An error occured: " + response.message);
                    }
                }
            });
        });
    });
    var clientname = document.getElementById("clientname");
    clientname.oninput = save;
    var box = document.getElementById("box");
    box.oninput = save;
    var text = document.getElementById("text");
    text.oninput = save;
    var check1 = document.getElementById("check1");
    check1.onchange = save;
    //поиск всех элементов формы, запоминание на всю жизнь страницы
    //и присваивание обработчика изменений

    clientname.value = localStorage.getItem("clientname");
    box.value = localStorage.getItem("box");
    text.value = localStorage.getItem("text");
    var q = localStorage.getItem("check1");
    if (q == 1) {
        check1.checked = true;
    }
    if (q == 0) {
        check1.checked = false;
    }
    //восстановление значений

    var popup = document.querySelector(".popup__overlay");
    var btn = document.getElementById("button2");
    var close = document.querySelector(".close");

    btn.addEventListener("click", function (event) {
        event.preventDefault();
        popup.classList.remove("hidden");
        window.history.pushState({
            page: 1
        }, "Main", "/lab8");
        window.history.pushState({
            page: 2
        }, "Form", "/lab8/#form");
        //history.replaceState({page: 3}, "title 3", "?page=3");
        //window.history.pushState({page: 'form'}, '', '/form');
    });
    //вызов окна по кнопке

    popup.addEventListener("click", function (event) {
        e = event || window.event;
        if (e.target == this) {
            popup.classList.add("hidden");
            window.history.back();
        }
    });
    //нажатие за пределы формы

    close.addEventListener("click", function (event) {
        event.preventDefault();
        popup.classList.add("hidden");
        window.history.back();
    });
    //по нажатии на кнопку

    window.addEventListener("popstate", function (event) {
        if ((document.location.toString().indexOf("form") + 1) == 0) {
            popup.classList.add("hidden");
        } else {
            popup.classList.remove("hidden");
        }
        //console.log(document.location.toString().indexOf("form")+1);
    });

});

function save() {
    //let text = document.getElementById("text");
    localStorage.setItem("clientname", clientname.value);
    //console.log(clientname.value);
    localStorage.setItem("box", box.value);
    localStorage.setItem("text", text.value);
    if (check1.checked) {
        localStorage.setItem("check1", 1);
    } else {
        localStorage.setItem("check1", 0);
    }
    //сохранение при любом изменении полей

}
